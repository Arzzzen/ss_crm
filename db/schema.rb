# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160504205904) do

  create_table "activities", force: :cascade do |t|
    t.integer  "trackable_id",   limit: 4
    t.string   "trackable_type", limit: 255
    t.integer  "owner_id",       limit: 4
    t.string   "owner_type",     limit: 255
    t.string   "key",            limit: 255
    t.text     "parameters",     limit: 65535
    t.integer  "recipient_id",   limit: 4
    t.string   "recipient_type", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "activities", ["owner_id", "owner_type"], name: "index_activities_on_owner_id_and_owner_type", using: :btree
  add_index "activities", ["recipient_id", "recipient_type"], name: "index_activities_on_recipient_id_and_recipient_type", using: :btree
  add_index "activities", ["trackable_id", "trackable_type"], name: "index_activities_on_trackable_id_and_trackable_type", using: :btree

  create_table "assets", force: :cascade do |t|
    t.integer  "object_id",          limit: 4
    t.string   "object_type",        limit: 255
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.string   "image_file_name",    limit: 255
    t.string   "image_content_type", limit: 255
    t.integer  "image_file_size",    limit: 4
    t.datetime "image_updated_at"
  end

  add_index "assets", ["object_id"], name: "index_assets_on_object_id", using: :btree
  add_index "assets", ["object_type"], name: "index_assets_on_object_type", using: :btree

  create_table "bc_metro_distances", force: :cascade do |t|
    t.integer  "business_center_id", limit: 4
    t.integer  "metro_id",           limit: 4
    t.string   "distance",           limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "bc_metro_distances", ["business_center_id", "metro_id"], name: "index_bc_metro_distances_on_business_center_id_and_metro_id", unique: true, using: :btree
  add_index "bc_metro_distances", ["business_center_id"], name: "index_bc_metro_distances_on_business_center_id", using: :btree
  add_index "bc_metro_distances", ["metro_id"], name: "index_bc_metro_distances_on_metro_id", using: :btree

  create_table "business_center_regions", force: :cascade do |t|
    t.integer "business_center_id", limit: 4, null: false
    t.integer "region_id",          limit: 4, null: false
  end

  add_index "business_center_regions", ["business_center_id", "region_id"], name: "business_centers_regions", unique: true, using: :btree

  create_table "business_center_services", force: :cascade do |t|
    t.integer "business_center_id", limit: 4,   null: false
    t.integer "service_id",         limit: 4,   null: false
    t.string  "desc",               limit: 255
  end

  add_index "business_center_services", ["business_center_id", "service_id"], name: "business_center_services", unique: true, using: :btree

  create_table "business_centers", force: :cascade do |t|
    t.string   "title",           limit: 255
    t.datetime "created_at",                                                         null: false
    t.datetime "updated_at",                                                         null: false
    t.integer  "grade",           limit: 1
    t.string   "address",         limit: 255
    t.text     "short_desc",      limit: 65535
    t.text     "description",     limit: 65535
    t.text     "characteristics", limit: 65535
    t.string   "percent",         limit: 255
    t.decimal  "latitude",                      precision: 10, scale: 8
    t.decimal  "longitude",                     precision: 11, scale: 8
    t.integer  "percent_color",   limit: 1
    t.text     "contacts",        limit: 65535
    t.integer  "city_id",         limit: 4
    t.datetime "parsed_at"
    t.string   "phone",           limit: 255
    t.string   "slug",            limit: 255
    t.string   "meta_desc",       limit: 255
    t.string   "meta_keyw",       limit: 255
    t.integer  "sort",            limit: 4,                              default: 0
    t.string   "street",          limit: 255
    t.string   "house_number",    limit: 255
    t.string   "house_case",      limit: 255
    t.string   "house_letter",    limit: 255
  end

  add_index "business_centers", ["city_id"], name: "index_business_centers_on_city_id", using: :btree

  create_table "cities", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "lead_room_types", force: :cascade do |t|
    t.integer  "lead_id",      limit: 4
    t.integer  "room_type_id", limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "lead_room_types", ["lead_id", "room_type_id"], name: "index_lead_room_types_on_lead_id_and_room_type_id", unique: true, using: :btree

  create_table "lead_sources", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "leads", force: :cascade do |t|
    t.string   "title",          limit: 255
    t.integer  "footage",        limit: 4
    t.text     "contact",        limit: 65535
    t.text     "requirements",   limit: 65535
    t.text     "plan",           limit: 65535
    t.integer  "lead_source_id", limit: 4
    t.integer  "manager_id",     limit: 4
    t.integer  "status_id",      limit: 4,     default: 1
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.text     "history",        limit: 65535
    t.date     "statusdate"
    t.integer  "phone_repeat",   limit: 4
    t.integer  "city_id",        limit: 4
  end

  add_index "leads", ["city_id"], name: "index_leads_on_city_id", using: :btree
  add_index "leads", ["lead_source_id"], name: "index_leads_on_lead_source_id", using: :btree
  add_index "leads", ["manager_id"], name: "index_leads_on_manager_id", using: :btree

  create_table "metros", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.integer  "region_id",  limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "notifications", force: :cascade do |t|
    t.datetime "notify_at"
    t.text     "text",       limit: 65535
    t.integer  "lead_id",    limit: 4
    t.integer  "user_id",    limit: 4
    t.integer  "creator_id", limit: 4
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.boolean  "showed",     limit: 1,     default: false
  end

  add_index "notifications", ["creator_id"], name: "index_notifications_on_creator_id", using: :btree
  add_index "notifications", ["lead_id"], name: "index_notifications_on_lead_id", using: :btree
  add_index "notifications", ["user_id"], name: "index_notifications_on_user_id", using: :btree

  create_table "pages", force: :cascade do |t|
    t.string   "title",            limit: 255
    t.text     "content",          limit: 65535
    t.string   "slug",             limit: 255
    t.string   "meta_description", limit: 255
    t.string   "meta_keywords",    limit: 255
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  create_table "parse_sources", force: :cascade do |t|
    t.integer  "business_center_id", limit: 4
    t.string   "url",                limit: 255
    t.boolean  "active",             limit: 1
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "parse_sources", ["business_center_id"], name: "index_parse_sources_on_business_center_id", using: :btree

  create_table "regions", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "room_types", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "rooms", force: :cascade do |t|
    t.integer  "business_center_id", limit: 4
    t.float    "area",               limit: 53
    t.float    "price",              limit: 53
    t.string   "floor",              limit: 255
    t.integer  "room_type_id",       limit: 4
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
    t.boolean  "parsed",             limit: 1,   default: false
  end

  add_index "rooms", ["business_center_id"], name: "index_rooms_on_business_center_id", using: :btree

  create_table "services", force: :cascade do |t|
    t.string   "title",      limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "class_name", limit: 255
  end

  create_table "statuses", force: :cascade do |t|
    t.string  "name",  limit: 255
    t.string  "color", limit: 50
    t.integer "sort",  limit: 1,   default: 0
  end

  create_table "user_cities", force: :cascade do |t|
    t.integer  "user_id",    limit: 4
    t.integer  "city_id",    limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "user_cities", ["city_id"], name: "index_user_cities_on_city_id", using: :btree
  add_index "user_cities", ["user_id"], name: "index_user_cities_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "first_name",             limit: 255
    t.string   "last_name",              limit: 255
    t.boolean  "is_admin",               limit: 1
    t.string   "avatar_file_name",       limit: 255
    t.string   "avatar_content_type",    limit: 255
    t.integer  "avatar_file_size",       limit: 4
    t.datetime "avatar_updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "bc_metro_distances", "business_centers", name: "fk_business_center_id", on_delete: :cascade
  add_foreign_key "bc_metro_distances", "metros", name: "fk_metro_id", on_delete: :cascade
  add_foreign_key "business_centers", "cities"
  add_foreign_key "leads", "cities"
  add_foreign_key "notifications", "users"
  add_foreign_key "user_cities", "users"
  add_foreign_key "user_cities", "users", on_delete: :cascade
end
