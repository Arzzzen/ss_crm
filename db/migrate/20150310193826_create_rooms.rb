class CreateRooms < ActiveRecord::Migration
  def change
    create_table :rooms do |t|
   		t.references :business_center, index: true
      t.integer :area
      t.decimal :price
      t.string :floor
      t.integer :type

      t.timestamps null: false
    end
  end
end
