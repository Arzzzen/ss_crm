class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.datetime :notify_at
      t.text :text
      t.references :lead, index: true
      t.references :user, index: true
      t.references :creator, references: :user, index: true

      t.timestamps null: false
    end
    add_foreign_key :notifications, :users
  end
end
