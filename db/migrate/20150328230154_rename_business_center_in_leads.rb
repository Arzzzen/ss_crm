class RenameBusinessCenterInLeads < ActiveRecord::Migration
  def change
  	rename_column :leads, :business_center_id, :lead_source_id
  end
end
