class AddParedAtToBusinessCenter < ActiveRecord::Migration
  def change
    add_column :business_centers, :parsed_at, :datetime
  end
end
