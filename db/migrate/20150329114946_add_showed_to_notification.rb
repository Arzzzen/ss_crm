class AddShowedToNotification < ActiveRecord::Migration
  def change
    add_column :notifications, :showed, :boolean, :default => 0
  end
end
