class AddHouseLetterToBusinessCenter < ActiveRecord::Migration
  def change
    add_column :business_centers, :house_letter, :string
  end
end
