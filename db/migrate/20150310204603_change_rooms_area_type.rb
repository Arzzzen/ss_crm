class ChangeRoomsAreaType < ActiveRecord::Migration
  def change
  	change_table:rooms do |t|
  		t.change :area, :double
  		t.change :price, :double
  	end
  end
end
