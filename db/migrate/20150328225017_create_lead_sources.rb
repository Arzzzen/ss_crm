class CreateLeadSources < ActiveRecord::Migration
  def change
    create_table :lead_sources do |t|
      t.string :title

      t.timestamps null: false
    end
  end
end
