class AddStatusdateToLead < ActiveRecord::Migration
  def change
    add_column :leads, :statusdate, :date
  end
end
