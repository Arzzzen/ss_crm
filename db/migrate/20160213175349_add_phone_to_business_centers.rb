class AddPhoneToBusinessCenters < ActiveRecord::Migration
  def change
    add_column :business_centers, :phone, :string
  end
end
