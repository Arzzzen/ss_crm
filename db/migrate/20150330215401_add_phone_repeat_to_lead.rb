class AddPhoneRepeatToLead < ActiveRecord::Migration
  def change
    add_column :leads, :phone_repeat, :integer
  end
end
