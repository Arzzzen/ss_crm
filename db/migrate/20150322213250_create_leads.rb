class CreateLeads < ActiveRecord::Migration
  def change
    create_table :leads do |t|
      t.string :title
      t.integer :footage
      t.text :contact
      t.text :requirements
      t.text :plan
      t.references :business_center, index: true
      t.references :manager, references: :user , index: true
      t.integer :status

      t.timestamps null: false
    end
  end
end
