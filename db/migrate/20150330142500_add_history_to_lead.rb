class AddHistoryToLead < ActiveRecord::Migration
  def change
    add_column :leads, :history, :text
  end
end
