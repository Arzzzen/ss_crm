class AddCityidToLead < ActiveRecord::Migration
  def change
    add_reference :leads, :city, index: true
    add_foreign_key :leads, :cities
  end
end
