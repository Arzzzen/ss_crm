class AddCityToBusinessCenter < ActiveRecord::Migration
  def change
    add_reference :business_centers, :city, index: true
    add_foreign_key :business_centers, :cities
  end
end
