class CreateBusinessCenterService < ActiveRecord::Migration
  def self.up
    # Create the association table
    create_table :business_center_services do |t|
      t.integer :business_center_id, :null => false
      t.integer :service_id, :null => false
    end

    # Add table index
    add_index :business_center_services, [:business_center_id, :service_id], :unique => true, :name => 'business_center_services'

  end

  def self.down
    remove_index :business_center_services, :column => [:business_center_id, :service_id], :name => 'business_center_services'
    drop_table :business_center_services
  end
end
