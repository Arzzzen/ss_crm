class AddMetaDescToBusinessCenters < ActiveRecord::Migration
  def change
    add_column :business_centers, :meta_desc, :string
  end
end
