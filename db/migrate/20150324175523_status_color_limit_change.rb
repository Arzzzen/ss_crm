class StatusColorLimitChange < ActiveRecord::Migration
  def change
  	change_column :statuses, :color, :string, :limit => 50
  end
end
