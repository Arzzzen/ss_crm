class AddHouseCaseToBusinessCenter < ActiveRecord::Migration
  def change
    add_column :business_centers, :house_case, :string
  end
end
