class AddFieldsToBusinessCenters < ActiveRecord::Migration
  def change
    add_column :business_centers, :grade, :integer, limit: 1, null: true
    add_column :business_centers, :phone, :string, null: true
    add_column :business_centers, :address, :string, null: true
    add_column :business_centers, :short_desc, :text, null: true
    add_column :business_centers, :desc, :text, null: true
    add_column :business_centers, :characteristics, :text, null: true
    add_column :business_centers, :percent, :integer, limit: 1, null: true
    add_column :business_centers, :latitude, :decimal, precision: 10, scale: 8, null: true
    add_column :business_centers, :longitude, :decimal, precision: 11, scale: 8, null: true
  end
end
