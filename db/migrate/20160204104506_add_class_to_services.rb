class AddClassToServices < ActiveRecord::Migration
  def change
    add_column :services, :class_name, :string
  end
end
