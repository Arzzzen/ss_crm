class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.string :title
      t.text :content
      t.string :slug
      t.string :meta_description
      t.string :meta_keywords

      t.timestamps null: false
    end
  end
end
