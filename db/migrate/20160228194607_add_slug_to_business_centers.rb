class AddSlugToBusinessCenters < ActiveRecord::Migration
  def change
    add_column :business_centers, :slug, :string
    add_index :business_centers, :slug, unique: true
  end
end
