class AddSortToBusinessCenters < ActiveRecord::Migration
  def change
    add_column :business_centers, :sort, :integer, default: 0
  end
end
