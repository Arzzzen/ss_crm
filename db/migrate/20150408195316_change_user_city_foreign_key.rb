class ChangeUserCityForeignKey < ActiveRecord::Migration
  def change
  	add_foreign_key :user_cities, :users, on_delete: :cascade
  end
end
