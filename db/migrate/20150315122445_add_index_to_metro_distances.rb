class AddIndexToMetroDistances < ActiveRecord::Migration
  def change
  	add_index :bc_metro_distances, ["business_center_id", "metro_id"], :unique => true
  	execute "ALTER TABLE bc_metro_distances ADD CONSTRAINT fk_metro_id FOREIGN KEY (metro_id) references metros(id) ON DELETE CASCADE;"
  	execute "ALTER TABLE bc_metro_distances ADD CONSTRAINT fk_business_center_id FOREIGN KEY (business_center_id) references business_centers(id) ON DELETE CASCADE;"
  end
end
