class AddIndexToStatus < ActiveRecord::Migration
  def change
    add_column :statuses, :sort, :integer, default: 0, limit: 1
  end
end
