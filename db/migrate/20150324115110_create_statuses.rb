class CreateStatuses < ActiveRecord::Migration
  def change
    create_table :statuses do |t|
      t.string :name, :null => true
      t.string :color, :limit => 6, :null => true
    end
  end
end
