class CreatesBusinessCentersRegions < ActiveRecord::Migration
  def self.up
    # Create the association table
    create_table :business_centers_regions, :id => false do |t|
      t.integer :business_center_id, :null => false
      t.integer :region_id, :null => false
    end

    # Add table index
    add_index :business_centers_regions, [:business_center_id, :region_id], :unique => true, :name => 'business_centers_regions'

  end

  def self.down
    remove_index :business_centers_regions, :column => [:business_center_id, :region_id], :name => 'business_centers_regions'
    drop_table :business_centers_regions
  end

end
