class CreateMetros < ActiveRecord::Migration
  def change
    create_table :metros do |t|
      t.string :title
      t.references :region

      t.timestamps null: false
    end
  end
end
