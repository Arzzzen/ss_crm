class AddContactsToBusinessCenter < ActiveRecord::Migration
  def change
    add_column :business_centers, :contacts, :text
    remove_column :business_centers, :phone
  end
end
