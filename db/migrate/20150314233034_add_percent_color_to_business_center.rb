class AddPercentColorToBusinessCenter < ActiveRecord::Migration
  def change
    add_column :business_centers, :percent_color, :integer, limit: 1
  end
end
