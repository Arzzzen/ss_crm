class AddStreetToBusinessCenter < ActiveRecord::Migration
  def change
    add_column :business_centers, :street, :string
  end
end
