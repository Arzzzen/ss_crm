class CreateAssets < ActiveRecord::Migration
  def change
    create_table :assets do |t|
      t.integer :object_id
      t.string :object_type

      t.timestamps null: false
    end
    add_index :assets, :object_id
    add_index :assets, :object_type
  end
end
