class AddHouseNumberToBusinessCenter < ActiveRecord::Migration
  def change
    add_column :business_centers, :house_number, :string
  end
end
