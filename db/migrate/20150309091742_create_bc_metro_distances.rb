class CreateBcMetroDistances < ActiveRecord::Migration
  def change
    create_table :bc_metro_distances, {:id => false, :force => true} do |t|
    	t.references :business_center, index: true
    	t.references :metro, index: true
    	t.string :distance

      t.timestamps
    end
  end
end
