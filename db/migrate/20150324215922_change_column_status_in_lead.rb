class ChangeColumnStatusInLead < ActiveRecord::Migration
  def change
  	change_column :leads, :status_id, :integer, :default => 1
  end
end
