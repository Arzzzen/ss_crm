class AddSourceToParseSources < ActiveRecord::Migration
  def change
    add_column :parse_sources, :source, :integer, limit: 1
  end
end
