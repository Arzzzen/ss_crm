class ChangeBcProcentType < ActiveRecord::Migration
  def change
  	change_table:business_centers do |t|
  		t.change :percent, :string
  	end
  end
end
