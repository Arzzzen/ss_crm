class AddParsedToRoom < ActiveRecord::Migration
  def change
    add_column :rooms, :parsed, :boolean, default: false
  end
end
