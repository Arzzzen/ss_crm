class CreateParseSources < ActiveRecord::Migration
  def change
    create_table :parse_sources do |t|
      t.references :business_center, index: true
      t.string :url
      t.boolean :active

      t.timestamps null: true
    end
  end
end
