class RenameRoomType < ActiveRecord::Migration
  def change
  	rename_column :rooms, :type, :room_type_id
  end
end
