class ChangeSlugUniq < ActiveRecord::Migration
  def change
    remove_index :business_centers, :slug
  end
end
