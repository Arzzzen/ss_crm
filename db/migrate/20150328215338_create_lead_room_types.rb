class CreateLeadRoomTypes < ActiveRecord::Migration
  def change
    create_table :lead_room_types do |t|
      t.references :lead
      t.references :room_type

      t.timestamps null: false

    end
    add_index(:lead_room_types, [:lead_id, :room_type_id], unique: true)
  end
end
