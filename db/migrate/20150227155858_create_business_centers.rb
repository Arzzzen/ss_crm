class CreateBusinessCenters < ActiveRecord::Migration
  def change
    create_table :business_centers do |t|
      t.string :title

      t.timestamps null: false
    end
  end
end
