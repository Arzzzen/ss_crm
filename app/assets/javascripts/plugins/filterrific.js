(function() {
	var Filterrific = {};

	$.fn.filterrific_observe_field = function(frequency, callback) {
	  frequency = frequency * 1000; // translate to milliseconds

	  return this.each(function(){
	    var $this = $(this);
	    var prev = $.isArray($this.val()) ? $this.val().toString() : $this.val();
	    var prevChecked = $this.prop('checked');

	    var check = function() {
	      if(removed()){ // if removed clear the interval and don't fire the callback
	        if(ti) clearInterval(ti);
	        return;
	      }

	      var val = $.isArray($this.val()) ? $this.val().toString() : $this.val();
	      var checked = $this.prop('checked');
	      if(prev != val || checked != prevChecked ){
	        prev = val;
	        prevChecked = checked;
	        $this.map(callback); // invokes the callback on $this
	      }
	    };

	    var removed = function() {
	      return $this.closest('html').length == 0
	    };

	    var reset = function() {
	      if(ti){
	        clearInterval(ti);
	        ti = setInterval(check, frequency);
	      }
	    };

	    check();
	    var ti = setInterval(check, frequency); // invoke check periodically

	    // reset counter after user interaction
	    $this.bind('keyup click mousemove', reset); //mousemove is for selects
	  });

	};

	var xhr;

	Filterrific.submitFilterForm = function(){
	  var form = $(this).parents("form"),
	      url = form.attr("action");
	  // turn on spinner
	  $('.filterrific_spinner').show();
	  // Submit ajax request
	  if (xhr) {
	    xhr.abort();
	  }
	  xhr = $.ajax({
	    url: url,
	    data: form.serialize(),
	    type: 'GET',
	    dataType: 'script'
	  }).done(function( msg ) {
	    $('.filterrific_spinner').hide();
	    mediator.publish('filter:updated');
	  });

	};

	$(document).on(
	  "change",
	  "#filterrific_filter :input",
	  Filterrific.submitFilterForm
	);

	$(".filterrific-periodically-observed").filterrific_observe_field(
	  0.5,
	  Filterrific.submitFilterForm
	);
})()

$(function() {
	var initFilter = function() {
	  var $filter = $('#fixed-filter');

	  if ($.cookie('filter_toggled') === undefined) {
	    $.cookie('filter_toggled', 'true');
	  }

	  $('.toggle', $filter).on('click', function() {
	    var width = $filter.css('left') == '0px' ? -$filter.outerWidth()+'px' : '0px';
	    $filter
	      .stop()
	      .animate({left: width})
	      .promise()
	      .done(function() {
	        $.cookie('filter_toggled', $.cookie('filter_toggled') === 'false' ? 'true' : 'false');
	      });
	  });

	  if ($filter.offset().top + $filter.outerHeight(true) > $(window).height()) {
	    var filterHeightWithoutScroler = $filter.outerHeight() - $('.scroller', $filter).outerHeight();
	    $filter
	      .css('top', '10px')
	      .find('.scroller, .slimScrollDiv')
	      .css('height', ($(window).height() - 20 - filterHeightWithoutScroler) + 'px');
	  }

	  if ($filter.outerWidth() + $('.toggle', $filter).outerHeight() > $(window).width()) {
	    $filter
	      .css('width', ($(window).width() - 20 - $('.toggle', $filter).outerHeight())+'px' );
	  }

	  if ($.cookie('filter_toggled') === "false") {
	    $filter.css('left', -$filter.outerWidth()+'px')
	  }
	  $filter.css('opacity', 1);

	  $('button[type=submit], .remove', $filter).on('click', function() {
	  	$.cookie('filter_toggled', 'false');
	  });
	}

	if ($('#fixed-filter').length) {
	  initFilter();
	}
})