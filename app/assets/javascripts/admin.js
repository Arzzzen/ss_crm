//= require jquery.flot
//= require jquery.flot.categories
//= require jquery.flot.pie

var initBarCharts = function() {

  var options = {
		series: {
			bars: {
				show: true,
				barWidth: 0.6,
				align: "center",
				lineWidth: 0
			}
		},

    grid: {
        tickColor: "#eee",
        borderColor: "#eee",
        borderWidth: 2
    },

		xaxis: {
			mode: "categories",
			tickLength: 0
		}
  };

  if ($('#leads_by_status').size() !== 0) {
    $.plot($("#leads_by_status"), [{
      data: gon.leads_by_status,
      lines: {
          lineWidth: 1,
      },
      shadowSize: 0
    }], options);
  }

  if ($('#leads_by_day').size() !== 0) {
    $.plot($("#leads_by_day"), [{
	      data: gon.leads_by_day,
	      lines: {
	          lineWidth: 1,
	      },
	      shadowSize: 0
	    }],
	    options
    );
  }

  if ($('#lead_sources_by_clients').size() !== 0) {
  	var labelFormatter = function(label, series) {
					return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
				}
			, showPie = function(data) {
			  	$.plot($("#lead_sources_by_clients"),
			  		data,
			  		{
			  			series: {
			  				pie: {
			  					show:true,
			  					// label: {
			       //        show: true,
			       //        radius: 1/3,
			       //        formatter: labelFormatter,
			       //        background: { 
			       //            opacity: 0.5,
			       //            color: '#000'
			       //        }
			       //      }
			  				}
			  			},
			  			legend: {
			  				show: false
			  			}
			  		}
			  	);
				};

		$('#lead_sources input').on('change', function() {
			var data = []
				, checked = [];
			$('#lead_sources input:checked').each(function() { checked.push($(this).val()) });
			$(checked).each(function(i, v) {
				data.push($.grep(gon.lead_sources_by_clients, function(data) {
					return data.label == v;
				})[0]);
			});
			showPie(data);
		});
		$('#lead_sources input:first').trigger('change');
  }
}

$(function() {
	initBarCharts();	
})