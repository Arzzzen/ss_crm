module LeadsHelper
	def xeditable? object = nil
	  true
	end

	def select_search(string, query = nil)
		query = query.to_s
		string = string.to_s
		if not query.blank?
			string = string.gsub(/#{query}/i, '<mark>\0</mark>').html_safe
		end
		string.gsub("\n", "<br />")
	end
end
