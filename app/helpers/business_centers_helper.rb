module BusinessCentersHelper
  require 'morpher'

  def fillDescription
    PublicActivity.enabled = false
    ac = ActionController::Base.new()
    regionsDecl = declinedRegions
    BusinessCenter.all.each { |bc|

      metroSentence = enumerationSentence(bc.metros, lambda{ |metros|
        metros.pluck(:title)
      })
      if metroSentence
        metroSentence = "#{['БЦ', 'ДЦ', 'Деловой центр', 'Бизнес центр'].sample} #{bc.title} #{['расположен', ' находится', 'располагается'].sample} #{['в шаговой', 'в транспортной'].sample} доступности от метро " + metroSentence + '.'
      end

      servicesSentence = enumerationSentence(bc.business_center_service, lambda{ |business_center_services|
        business_center_services.map { |business_center_service|
          business_center_service.title + (business_center_service.desc ? " (#{business_center_service.desc})" : '')
        }
      })
      if not servicesSentence.empty?
        servicesSentence = "В бизнес центре есть: #{servicesSentence}."
      end

      min_area = bc.rooms.pluck(:area).map(&:to_i).min.to_i
      max_area = bc.rooms.pluck(:area).map(&:to_i).max.to_i
      if min_area > 3 || max_area > 10
        min_area = min_area > 3 ? " от #{min_area} м" : ''
        max_area = max_area > 10 ? " до #{max_area} м" : ''
        areaSentence = " В аренду #{['предлагаются', 'предоставляются'].sample} #{['помещения', 'офисы'].sample}#{min_area+max_area}."
      else
        areaSentence = ''
      end

      rendered = ac.render_to_string 'business_centers/boilerplate/description',
        :layout => false,
        :locals => {
          bc: bc,
          regionsDecl: regionsDecl,
          metroSentence: metroSentence,
          servicesSentence: servicesSentence,
          areaSentence: areaSentence
        }
      bc.description = rendered
      bc.save
    }
    PublicActivity.enabled = true
  end

  def declinedRegions
    regionsDecl = {}
    Region.all.each { |region|
      regionsDecl[region.id] = Morpher.new(region.title)
    }
    return regionsDecl
  end

  def enumerationSentence(elements, stringify)
    if elements.any?
      stringified = stringify.call(elements)
      if elements.count == 1
        metroSentence = stringified.first
      else
        metroSentence = stringified.first(stringified.size-1).join(', ')+' и '+stringified.last
      end
    else
      metroSentence = ''
    end
    return metroSentence
  end
end
