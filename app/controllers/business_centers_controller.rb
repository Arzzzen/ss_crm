class BusinessCentersController < ApplicationController
  before_action :set_business_center, only: [:show, :edit, :update, :destroy, :parse]
  skip_before_filter :verify_authenticity_token, :only => [:index, :show]
  before_filter :authorize, :only => [:destroy, :import]
  skip_before_filter :authenticate_user!, :only => [:zipal, :cian, :emls, :irr]

  # GET /business_centers
  # GET /business_centers.json
  def index
    if not params[:callback] then
      @filterrific = initialize_filterrific(
        BusinessCenter,
        params[:filterrific],
        select_options: {
          with_region_id: Region.options_for_select,
          with_grade: BusinessCenter.grades,
          with_rooms_type: RoomType.options_for_select
        }
      ) or return
      respond_to do |format|
          @business_centers = @filterrific.find
            .for_cities(current_user.show_city(session))
            .eager_load(:regions, :images, :metros, :services,
                {:rooms => :room_type, :bc_metro_distances => :metro}
                )
            .page(params[:page])
          @total = BusinessCenter.count
          @page_title = "Список бизнес центров <small>(#{@total.to_s})</small>"
          format.html
          format.js
          format.csv { render text: BusinessCenter.for_cities(current_user.show_city(session)).to_csv }
          format.xls do
            @business_centers = BusinessCenter.for_cities(current_user.show_city(session))
          end
      end
    else
      respond_to do |format|
        format.js do
          @filterrific = initialize_filterrific(
            BusinessCenter,
            nil,
            select_options: {
              with_region_id: Region.options_for_select,
              with_grade: BusinessCenter.grades,
              with_rooms_type: RoomType.options_for_select
            }
          ) or return
          bbox = params[:bbox].split(',')
          @business_centers = @filterrific.find.for_cities(current_user.show_city(session)).where({ :latitude => bbox[0]..bbox[2], :longitude => bbox[1]..bbox[3]})

          result = {
            'type' => "FeatureCollection",
            'features' => Array.new
          }
          @business_centers.each do |bc|
            result['features'] << business_center_map_object(bc)
          end
          render :json => result, :callback => params[:callback]
        end
      end
    end

  end

  # GET /business_centers/1
  # GET /business_centers/1.json
  def show
    respond_to do |format|
      format.js do
        result = {
          'type' => "FeatureCollection",
          'features' => [ business_center_map_object(@business_center) ]
        }

        render :json => result, :callback => params[:callback]
      end
      format.html
    end
  end

  # GET /business_centers/new
  def new
    @page_title = 'Добавить бизнес центр'
    @business_center = BusinessCenter.new
    render :layout => !request.xhr?
  end

  # GET /business_centers/1/edit
  def edit
    @page_title = 'Редактировать бизнес центр #'+@business_center.id.to_s
    render :layout => !request.xhr?
  end

  # POST /business_centers
  # POST /business_centers.json
  def create
    @business_center = BusinessCenter.new(business_center_params)

    respond_to do |format|
      if @business_center.save
        format.html { redirect_to params[:save_and_edit] ? edit_business_center_path(@business_center) : business_centers_path, notice: 'Бизнес центр был успешно создан.' }
        format.json { render :show, status: :created, location: @business_center }
      else
        format.html { render :new }
        format.json { render json: @business_center.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /business_centers/1
  # PATCH/PUT /business_centers/1.json
  def update
    respond_to do |format|
      if @business_center.update(business_center_params)
        format.html { redirect_to params[:save_and_edit] ? edit_business_center_path(@business_center) : business_centers_path, notice: 'Бизнес центр был успешно обновлен.' }
        format.json { render json: {
          status: :ok,
          tr: render_to_string(partial: 'element.html', locals: { business_center: @business_center }),
          id: @business_center.id,
          notice: 'Бизнес центр был успешно обновлен.' } }
      else
        format.html { render :edit }
        format.json { render json: @business_center.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /business_centers/1
  # DELETE /business_centers/1.json
  def destroy
    @business_center.destroy
    respond_to do |format|
      format.html { redirect_to business_centers_url, notice: 'Бизнес центр был успешно удален.' }
      format.json { head :no_content }
    end
  end

  def import
    BusinessCenter.for_cities(current_user.show_city(session)).import(params[:file])
    redirect_to admin_url, notice: "Бизнес центры успешно импортированы."
  end

  def zipal
    respond_to do |format|
      format.xml do
        @rooms = Room.joins(:business_center)
      end
    end
  end

  def cian
    respond_to do |format|
      format.xml do
        @rooms = Room.where('area > ?', 80).joins(:business_center).where('percent > ?', 50)
      end
    end
  end

  def emls
    respond_to do |format|
      format.xml do
        @rooms = Room
          .emls_region
          .where('area > ?', 80)
          .where('percent > ?', 50)
      end
    end
  end

  def parse
    parsed = @business_center.parse
    result = parsed[:rooms].as_json({methods: :type})
    render json: {success: result}
  end

  def parse_test
    if (( url = params[:url] ) && (bc_id = params[:bc_id]) )
      parse_source = ParseSource.new
      parse_source.business_center_id = bc_id
      parse_source.url = url
      parse_source.active = true
      parsed = parse_source.parse
      result = parsed[:rooms].as_json({methods: :type})
      render json: {success: result}
    else
      render json: {error: 'Ссылка должна быть заполнена'}, status: 403
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_business_center
      @business_center = BusinessCenter.for_cities(current_user.show_city(session)).find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def business_center_params
      params.require(:business_center).permit(
        :title, :address, :description, :short_desc, :grade, :characteristics, :percent, :percent_color, :contacts, :city_id, :latitude, :longitude, :phone, :slug, :meta_desc, :meta_keyw, :sort,
        :images_attributes => [:id, :image, :_destroy],
        :business_center_region_attributes => [:id, :region_id, :_destroy],
        :bc_metro_distances_attributes => [:id, :metro_id, :distance, :_destroy],
        :rooms_attributes => [:id, :area, :price, :floor, :room_type_id, :_destroy],
        :parse_sources_attributes => [:id, :url, :active, :_destroy],
        :business_center_service_attributes => [:id, :service_id, :desc, :_destroy]
        )
    end

    def business_center_map_object(bc)
      link = view_context.link_to(bc, {target: "_blank", class: 'link_no_style'}) do
        ('<span class="font-red-thunderbird">'+bc.title[0]+'</span>'+bc.title[1..-1]).html_safe
      end
      {
        "type" => "Feature",
        "id" => bc.id,
        "geometry" => {
          "type" => "Point",
          "coordinates" => [bc.latitude, bc.longitude]
        },
        "properties" => {
          "balloonContent" => "<h4>"+link+" <span class=\"label label-info\">#{bc.grade}</span></h4><p>#{bc.address}</p>",
          "clusterCaption" => bc.title,
          "hintContent" => bc.title
        }
      }
    end
end
