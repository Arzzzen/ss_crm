class ApiController < ApplicationController
  skip_before_filter :authenticate_user!
  before_filter :set_access

  def set_access
    headers["Access-Control-Allow-Origin"] = "*"
  end

  def business_centers
    filterrific = initialize_filterrific(
      BusinessCenter,
      (params['s'].is_a? Hash) ? params['s'] : {}
    ) or return
    include_only = [:id, :title, :address, :grade, :latitude, :longitude, :phone, :slug]
    business_centers = filterrific.find
      .select(include_only)
      .eager_load(:regions, :images, {:rooms => :room_type}, :bc_metro_distances, :business_center_service)
      .page(params[:page])
      .per(params[:per_page] || 5)
      .reorder({:sort => :desc, :title => :asc})

    if (params['s'] && params['s'][:city]) then
      business_centers = business_centers.for_cities({params['s'][:city] => params['s'][:city]})
    end

    bcs_json = business_centers.map{ |bc|
      images = bc.images.map{ |image|
        URI.join(request.url, image.image.url(:api_thumb)).to_s # '//crm.sim-sol.ru'+image.image.url(:api_thumb)
      }
      images = ['//crm.sim-sol.ru'+ActionController::Base.helpers.asset_path('no-photo-bc.jpg')] if (images.length == 0) #request.url
      bc
        .as_json({
          :only => include_only,
          :include => {
            :bc_metro_distances => {:only => [:metro_id, :distance]},
            :rooms => {
              :only => [:id, :area, :price, :floor],
              :methods => [:type]
            },
            :regions => {:only => [:id]},
            :business_center_service => {
              :only => [:service_id, :desc]
            }
          }
        })
        .merge({ :images => images})
    }

    respond_to do |format|
      format.json { render :json => bcs_json }
    end
  end
  def business_center
    include_only = [
      :id,
      :title,
      :address,
      :grade,
      :latitude,
      :longitude,
      :phone,
      :description,
      :slug,
      :meta_desc,
      :meta_keyw
    ]

    bc = BusinessCenter
      .select(include_only)
      .eager_load(:regions, :images, {:rooms => :room_type}, :bc_metro_distances, :business_center_service).
      find(params[:id])

    images_full = bc.images.map{ |image|
      URI.join(request.url, image.image.url(:api_full)).to_s # '//crm.sim-sol.ru'+image.image.url(:api_thumb)
    }

    bc_json = bc
      .as_json({
        :only => include_only,
        :include => {
          :bc_metro_distances => {:only => [:metro_id, :distance]},
          :rooms => {
            :only => [:id, :area, :price, :floor],
            :methods => [:type]
          },
          :regions => {:only => [:id]},
          :business_center_service => {
            :only => [:service_id, :desc]
          }
        }
      })
      .merge({
        full: true,
        images_full: images_full
      })

    respond_to do |format|
      format.json { render :json => bc_json }
    end
  end
  def cities
    cities = City.all
    respond_to do |format|
      format.json { render :json => cities }
    end
  end
  def regions
    regions = Region.all
    respond_to do |format|
      format.json { render :json => regions.as_json({only: [:id, :title]}) }
    end
  end
  def metros
    metros = Metro.all
    respond_to do |format|
      format.json {
        render :json => metros.as_json({
          methods: [:color],
          only: [:id, :title, :region_id]
        }) 
      }
    end
  end
  def room_types
    roomTypes = RoomType.all
    respond_to do |format|
      format.json {
        render :json => roomTypes.as_json({
          only: [:id, :title]
        }) 
      }
    end
  end
  def services
    services = Service.all
    respond_to do |format|
      format.json {
        render :json => services.as_json({
          only: [:id, :title, :class_name]
        }) 
      }
    end
  end
  def business_centers_for_map
    bc = BusinessCenter.select('id, title, address, latitude, longitude')

    bcs_json = bc
      .as_json({
        :include => {
          :bc_metro_distances => {:only => [:metro_id]}
        }
      })

    respond_to do |format|
      format.json { render :json => bcs_json }
    end
  end
  def page
    include_only = [
      :id,
      :title,
      :meta_description,
      :meta_keywords,
      :content
    ]

    page = Page
      .select(include_only)
      .find(params[:id])

    page_json = page
      .as_json({
        :only => include_only
      })

    respond_to do |format|
      format.json { render :json => page_json }
    end
  end
end
