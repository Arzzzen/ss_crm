class ApplicationController < ActionController::Base
	include PublicActivity::StoreController

  helper_method :admin?
  before_filter :authenticate_user!
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  layout Proc.new { |controller| controller.devise_controller? ? 'devise' : 'application' }

  def track_params(model_instance, rules)
    Hash[
      model_instance
        .changes
        .slice(*rules.keys)
        .map{ |k, v|
            if rules[k].nil?
              [k, v]
            else
              [k, v.map{ |vi| rules[k].call(vi)} ]
            end
          }
      ]
  end


  protected

    def admin?
      current_user.is_admin
    end

    def authorize
      unless admin?
        redirect_to root_path, alert: 'Доступ запрещен.'
        false
      end
    end
end
