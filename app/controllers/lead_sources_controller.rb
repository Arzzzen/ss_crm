class LeadSourcesController < ApplicationController
  before_action :set_lead_source, only: [:show, :edit, :update, :destroy]

  # GET /lead_sources
  def index
    @lead_sources = LeadSource.all
  end

  # GET /lead_sources/new
  def new
    @lead_source = LeadSource.new
    render :layout => !request.xhr?
  end

  # GET /lead_sources/1/edit
  def edit
    render :layout => !request.xhr?
  end

  # POST /lead_sources
  def create
    @lead_source = LeadSource.new(lead_source_params)

    if @lead_source.save
      redirect_to :back, notice: 'Источник килентов был успешно создан.'
    else
      render :new
    end
  end

  # PATCH/PUT /lead_sources/1
  def update
    if @lead_source.update(lead_source_params)
      redirect_to :back, notice: 'Источник килентов был успешно обновлен.'
    else
      render :edit
    end
  end

  # DELETE /lead_sources/1
  def destroy
    @lead_source.destroy
    redirect_to :back, notice: 'Источник килентов был успешно удален.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_lead_source
      @lead_source = LeadSource.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def lead_source_params
      params.require(:lead_source).permit(:title)
    end
end
