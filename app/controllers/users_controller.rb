class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_filter :authorize, :except => :change_showed_city

  # GET /users
  # GET /users.json
  def index
    @page_title = 'Список пользователей'
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  # GET /users/new
  def new
    @page_title = 'Добавить пользователя'
    @user = User.new
    render :layout => !request.xhr?
  end

  # GET /users/1/edit
  def edit
    @page_title = 'Редактировать пользователя'
    render :layout => !request.xhr?
  end

  # POST /users
  # POST /users.json
  def create
    @page_title = 'Добавить пользователя'
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to :back, notice: 'Пользователь был успешно создан.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to :back, notice: 'Пользователь был успешно обновлен.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to :back, notice: 'Пользователь был успешно удален.' }
      format.json { head :no_content }
    end
  end

  def change_showed_city
    if not params[:city_id].nil?
      current_user.change_showed_city(session, params[:city_id])
    end
    redirect_to :back
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      if params[:user][:password].blank? then
        params.require(:user).permit(:email, :first_name, :last_name, :is_admin, :avatar,
        :city_ids => []
        )
      else
        params.require(:user).permit(:email, :first_name, :password, :last_name, :is_admin, :avatar,
        :city_ids => []
        )
      end
    end
end
