class CitiesController < ApplicationController
  before_filter :authorize

  before_action :set_city, only: [:show, :edit, :update, :destroy]

  # GET /cities
  def index
    @cities = City.all
  end

  # GET /cities/new
  def new
    @city = City.new
    render :layout => !request.xhr?
  end

  # GET /cities/1/edit
  def edit
    render :layout => !request.xhr?
  end

  # POST /cities
  def create
    @city = City.new(city_params)

    if @city.save
      redirect_to :back, notice: 'Город был успешно создан.'
    else
      render :new
    end
  end

  # PATCH/PUT /cities/1
  def update
    if @city.update(city_params)
      redirect_to :back, notice: 'Город был успешно обновлен.'
    else
      render :edit
    end
  end

  # DELETE /cities/1
  def destroy
    @city.destroy
    redirect_to :back, notice: 'Город был успешно удален.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_city
      @city = City.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def city_params
      params.require(:city).permit(:title)
    end
end
