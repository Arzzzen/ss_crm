class LeadsController < ApplicationController
  before_action :set_lead, only: [:show, :edit, :update, :destroy]
  respond_to :html, :xml, :json, :csv, :xls
  before_filter :authorize, :only => [:destroy, :import]

  # GET /leads
  # GET /leads.json
  def index
    @filterrific = initialize_filterrific(
      Lead,
      params[:filterrific],
      select_options: {
        sorted_by: Lead.options_for_sorted_by
      } 
    ) or return
    @search_query = @filterrific.search_query
    respond_to do |format|
      @leads = @filterrific.find
        .joins(:status)
        .for_cities(current_user.show_city(session))
        .select('leads.*, statuses.color as status_color')
      if not params["with_contact_like"].blank?
        @leads = Lead
          .joins(:status)
          .with_contact_like(params["with_contact_like"])
          .for_cities(current_user.show_city(session))
          .select('leads.*, statuses.color as status_color')
      end
      @leads = @leads.page(params[:page])
      @page_title = "Список клиентов"
      format.html do
        @managers_for_filter = Lead.group('manager_id').select('first_name, manager_id, count(*) as counter').joins(:manager)
      end
      format.js
      format.csv { render text: Lead.for_cities(current_user.show_city(session)).to_csv }
      format.xls do
        @leads = Lead.for_cities(current_user.show_city(session))
      end
    end
  end

  # GET /leads/1
  # GET /leads/1.json
  def show
  end

  # GET /leads/new
  def new
    @lead = Lead.new
    render :layout => !request.xhr?
  end

  # GET /leads/1/edit
  def edit
    render :layout => !request.xhr?
  end

  # POST /leads
  # POST /leads.json
  def create
    @lead = Lead.new(lead_params)
    respond_to do |format|
      if @lead.save
        format.html { redirect_to params[:save_and_edit] ? edit_lead_path(@lead) : leads_path, notice: 'Клиент был успешно создан.' }
        format.json { render :show, status: :created, location: @lead }
      else
        format.html { render :new }
        format.json { render json: @lead.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /leads/1
  # PATCH/PUT /leads/1.json
  def update
    filtered_params = lead_params
    if not current_user.is_admin
      filtered_params.except!(*[:lead_source_id, :manager_id])
    end
    respond_to do |format|
      if @lead.update(filtered_params)
        format.html { redirect_to params[:save_and_edit] ? edit_lead_path(@lead) : leads_path, notice: 'Клиент был успешно обновлен.' }
        format.json do
          respond_with @lead
        end
      else
        format.html { render :edit }
        format.json { render json: @lead.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /leads/1
  # DELETE /leads/1.json
  def destroy
    @lead.destroy
    respond_to do |format|
      format.html { redirect_to leads_url, notice: 'Клиент был успешно удален.' }
      format.json { head :no_content }
    end
  end

  def import
    Lead.for_cities(current_user.show_city(session)).import(params[:file])
    redirect_to admin_url, notice: "Клиенты успешно импортированы."
  end

  def select
    respond_to do |format|
      format.js do
        render json: Lead.where('title LIKE ?', "%#{params[:q]}%").select(['title as text', :id])
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_lead
      @lead = Lead.for_cities(current_user.show_city(session)).find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def lead_params
      params.require(:lead).permit(:title, :footage, :contact, :requirements, :plan, :manager_id, :status_id, :lead_source_id, :history, :statusdate, :city_id,
        :room_type_ids => []
        )
    end
end
