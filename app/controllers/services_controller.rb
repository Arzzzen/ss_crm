class ServicesController < ApplicationController
  before_action :set_service, only: [:show, :edit, :update, :destroy]
  before_filter :authorize

  # GET /services
  def index
    @services = Service.all
  end

  # GET /services/new
  def new
    @service = Service.new
    render :layout => !request.xhr?
  end

  # GET /services/1/edit
  def edit
    render :layout => !request.xhr?
  end

  # POST /services
  def create
    @service = Service.new(service_params)

    if @service.save
      redirect_to :back, notice: 'Сервис был успешно создан.'
    else
      render :new
    end
  end

  # PATCH/PUT /services/1
  def update
    if @service.update(service_params)
      redirect_to :back, notice: 'Сервис был успешно обновлен.'
    else
      render :edit
    end
  end

  # DELETE /services/1
  def destroy
    @service.destroy
    redirect_to services_url, notice: 'Севрси был успешно удален.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_service
      @service = Service.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def service_params
      params.require(:service).permit(:title, :class_name)
    end
end
