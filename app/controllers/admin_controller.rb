class AdminController < ApplicationController
	before_filter :authorize

  def index
  	@page_title = 'Администрирование'
  end

  def statistics
  	@filterrific = initialize_filterrific(
  	  Lead,
  	  params[:filterrific]
  	) or return
  	leads = @filterrific.find.for_cities(current_user.show_city(session))
  	gon.leads_by_day = leads.group("YEAR(created_at), MONTH(created_at)")
  		.select("count(*) as data, created_at as label").map{|v| [v[:label].strftime("%Y-%m"), v[:data]] }
  	gon.leads_by_status = leads.group(:status).count.map{|k,v| [k[:name], v] }
  	@lead_sources_by_clients = leads.joins(:lead_source).group(:lead_source).count.map{|ls,v|
  		{label: ls.title , data: v}
  	}.sort_by { |hsh| hsh[:data] }.reverse
  	gon.lead_sources_by_clients = @lead_sources_by_clients
  end

  def parse
  end
end
