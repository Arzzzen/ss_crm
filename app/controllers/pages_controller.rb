class PagesController < ApplicationController
  before_action :set_page, only: [:show, :edit, :update, :destroy]
  before_filter :authorize

  # GET /pages
  def index
    @pages = Page.all
  end

  # GET /pages/new
  def new
    @page = Page.new
    render :layout => !request.xhr?
  end

  # GET /pages/1/edit
  def edit
    render :layout => !request.xhr?
  end

  # POST /pages
  def create
    @page = Page.new(page_params)

    if @page.save
      redirect_to :back, notice: 'Страница была успешно создана.'
    else
      render :new
    end
  end

  # PATCH/PUT /pages/1
  def update
    if @page.update(page_params)
      redirect_to :back, notice: 'Страница была успешно обновлена.'
    else
      render :edit
    end
  end

  # DELETE /pages/1
  def destroy
    @page.destroy
    redirect_to pages_url, notice: 'Страница была успешно удалена.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_page
      @page = Page.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def page_params
      params.require(:page).permit(:title, :content, :slug, :meta_description, :meta_keywords)
    end
end
