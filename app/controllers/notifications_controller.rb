class NotificationsController < ApplicationController
  before_action :set_notification, only: [:show, :edit, :update, :destroy]

  # GET /notifications
  def index
    respond_to do |format|
      format.json do
        notifications = Notification
          .where('showed = ?', false)
          .where('user_id = ?', current_user.id)
          .where('notify_at <= ?', Time.now)
        json_notifications = notifications.to_json(:include => :lead)
        notifications.update_all(:showed => true)
        render json: json_notifications
      end
      format.html do
        notifications = Arel::Table.new(:notifications)
        cond = notifications[:user_id].eq(current_user.id)
        cond = cond.or(notifications[:creator_id].eq(current_user.id)) if current_user.is_admin
        @notifications = Notification.where(cond).page(params[:page])
      end
    end
  end

  # GET /notifications/new
  def new
    @notification = Notification.new
    render :layout => !request.xhr?
  end

  # GET /notifications/1/edit
  def edit
    render :layout => !request.xhr?
  end

  # POST /notifications
  def create
    not_params = notification_params
    if current_user.is_admin && !params[:notification][:user_id].blank? then
      not_params[:user_id] = params[:notification][:user_id]
    else
      not_params[:user_id] = current_user.id
    end

    @notification = Notification.new(not_params)
    @notification.creator_id = current_user.id
    if @notification.save
      redirect_to params[:save_and_edit] ? edit_notification_path(@notification) : notifications_path, notice: 'Напоминание было успешно создано.'
    else
      render :new
    end
  end

  # PATCH/PUT /notifications/1
  def update
    not_params = notification_params
    if current_user.is_admin && !params[:notification][:user_id].blank? then
      not_params[:user_id] = params[:notification][:user_id]
    else
      not_params[:user_id] = current_user.id
    end
    not_params[:showed] = false if not_params[:notify_at] >= Time.now
    if @notification.update(not_params)
      redirect_to params[:save_and_edit] ? edit_notification_path(@notification) : notifications_path, notice: 'Напоминание было успешно обновлено.'
    else
      render :edit
    end
  end

  # DELETE /notifications/1
  def destroy
    if @notification.user_id == current_user.id || @notification.creator_id == current_user.id then
      @notification.destroy
      redirect_to notifications_url, notice: 'Напоминание было успешно удалено.'
    else
      redirect_to notifications_url, notice: 'Напоминание не может быть удалено вами.'
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_notification
      @notification = Notification.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def notification_params
      params.require(:notification).permit(:notify_at, :text, :lead_id, :creator_id)
    end
end
