class Region < ActiveRecord::Base
	has_and_belongs_to_many :business_centers

	default_scope { order('title')}

	def self.options_for_select
	  order('LOWER(title)').map { |e| [e.title, e.id] }
	end
end
