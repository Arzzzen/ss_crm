class Notification < ActiveRecord::Base
  belongs_to :lead
  belongs_to :user
  belongs_to :creator, :class_name => 'User'

  default_scope { order('notify_at desc') }
end
