class Page < ActiveRecord::Base
  after_create :slug_create

  def slug_create
    id = self.id
    if self.slug.nil? || self.slug.empty?
      self.slug = "#{id}-#{self.title.parameterize}"
    else
      self.slug = "#{id}-#{self.slug}"
    end
    self.save
  end
end
