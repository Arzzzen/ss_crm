class BusinessCenterService < ActiveRecord::Base
  belongs_to :business_center
  belongs_to :service

  def title
    self.service.title
  end

  def class_name
    self.service.class_name
  end
end