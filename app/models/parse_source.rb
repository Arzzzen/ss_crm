class ParseSource < ActiveRecord::Base
	belongs_to :business_center

	scope :active, lambda {
		where(active: true)
	}

	def recognize_source
		self.source = !(self.url =~ /bcinform\.ru\//).nil?
		return true
	end

	def parse
		require 'open-uri'
		require 'nokogiri'
		rooms = Array.new
		business_center_service = Array.new
		result = {rooms: Array.new, business_center_service: Array.new}
		begin
		  page = Nokogiri::HTML(open(self.url.to_s))
		rescue OpenURI::HTTPError => e
		  return result
		end
		if self.url =~ /bcinform\.ru/
			page.css('.myTableResponsive tr[idss]').each do |tr|
				rooms << {
					floor: tr.css('td')[2].inner_text.split.join,
					area: tr.css('td')[3].inner_text.split.join,
					price: tr.css('td')[4].inner_text.split.join,
					room_type_id: RoomType.where(title: tr.css('td')[1].inner_text.split.join).first.try(:id)
				}
			end
			page.css('#serviceObject .objectService:not(.noService)').each do |ico|
				service_id = ico.css('.objectServiceIco').attr('id').to_s.gsub(/\D/, '').to_i
				desc = ico.css('.objectServiceDoubleText') ? ico.css('.objectServiceDoubleText').inner_text : nil
				business_center_service << {
					business_center_id: self.business_center_id,
					service_id: service_id,
					desc: desc
				}
			end
		elsif self.url =~ /biz-cen\.ru/
			page.css('.container-left-info-freeOffice-table.first tr[plan_id]').each do |tr|
				rooms << {
					floor: tr.css('td:nth-child(2)').inner_text.split.join,
					area: tr.css('td:nth-child(1)').inner_text.encode('UTF-8', 'binary', invalid: :replace, undef: :replace, replace: '').gsub(',', '.').to_f,
					price: tr.css('td:nth-child(3)').inner_text.encode('UTF-8', 'binary', invalid: :replace, undef: :replace, replace: '').gsub(/[^0-9.]/, "")
				}
			end
		elsif self.url =~ /6550101\.ru/
			page.css('#search_pt tr').each do |tr|
				floor = /(\d+) этаж/.match(tr.css('.name').inner_text).try(:[], 1)
				room_type_id = nil
				RoomType.all.each do |room_type|
					if tr.css('.name').inner_text.mb_chars.downcase.include? room_type[:title].mb_chars.downcase
						room_type_id = room_type[:id]
					end
				end
				rooms << {
					floor: floor,
					area: tr.css('.digits .s').inner_text.split.join,
					price: tr.css('.digits .stavka').inner_text.split.join,
					room_type_id: room_type_id
				}
			end
		end

		rooms.each do |room|
		  room = Room.new room
		  room.parsed = true
		 	result[:rooms] << room
		end
		business_center_service.each do |bcs|
		 	result[:business_center_service] << BusinessCenterService.new(bcs)
		end
		return result
	end
end
