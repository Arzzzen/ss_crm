class Lead < ActiveRecord::Base

  belongs_to :status
  belongs_to :manager, :class_name => 'User'
  has_many :lead_room_types
  has_many :room_types, :through => :lead_room_types
  belongs_to :city
  belongs_to :lead_source
  before_create :set_default_field_values
  before_save :clean_attributes
  after_save :phone_repeats_write
  after_destroy :phone_repeats_write

  filterrific(
    default_filter_params: { sorted_by: 'created_at_desc' },
    available_filters: [
      :sorted_by,
      :search_query,
      :with_manager_id,
      :with_created_at_between,
      :with_statusdate_between,
      :with_room_types,
      :with_footage_gt,
      :with_footage_lt
    ]
  )

  include PublicActivity::Model
  tracked owner: -> (controller, model) { controller && controller.current_user },
          params:{ "obj" => proc {|controller, model_instance| 
              controller.track_params(model_instance,
                {
                  "title" => nil,
                  "footage" => nil,
                  "contact" => nil,
                  "requirements" => nil,
                  "statusdate" => nil,
                  "history" => nil,
                  "plan" => nil,
                  "lead_source_id" => lambda { |lead_source_id|
                    LeadSource.find_by_id(lead_source_id).nil? ? '' : LeadSource.find(lead_source_id).title
                  },
                  "manager_id" =>  lambda { |manager_id|
                    User.find_by_id(manager_id).nil? ? '' : User.find(manager_id).first_name
                  },
                  "status_id" => lambda { |status_id|
                    Status.find_by_id(status_id).nil? ? '' : Status.find(status_id).name
                  },
                  "city_id" => lambda { |city_id|
                    City.find_by_id(city_id).nil? ? '' : City.find(city_id).title
                  }
                }
              )
            }
          }

	scope :search_query, lambda { |query|
    return nil if query.blank?
    query = "%#{query.to_s.downcase}%"
    where("LOWER(title) LIKE :query 
      OR LOWER(footage) LIKE :query 
      OR LOWER(contact) LIKE :query 
      OR LOWER(requirements) LIKE :query 
      OR LOWER(plan) LIKE :query 
      OR LOWER(history) LIKE :query", {query: query})
  }

  scope :sorted_by, lambda { |sort_option|
    # extract the sort direction from the param value.
    direction = (sort_option =~ /desc$/) ? 'desc' : 'asc'
    case sort_option.to_s
    when /^created_at/
      order("leads.created_at #{ direction }")
    when /^status/
      order("statuses.sort #{ direction }")
    when /^footage/
      order("leads.footage #{ direction }")
    when /^stdate/
      order("leads.statusdate #{ direction }")
    else
      raise(ArgumentError, "Invalid sort option: #{ sort_option.inspect }")
    end
  }

  scope :with_manager_id, lambda { |manager_id|
    return nil if manager_id.blank?
    where('manager_id' => manager_id)
  }

  scope :with_created_at_between, lambda { |between|
    return nil if between.blank?
    from = between.split(' - ')
    where(:created_at => from[0].to_datetime.beginning_of_day..from[1].to_datetime.end_of_day)
  }

  scope :with_room_types, lambda { |room_types|
    room_types.reject!(&:blank?)
    where('lead_room_types.room_type_id' => [*room_types]).joins(:lead_room_types).uniq if not room_types.empty?
  }

  scope :with_contact_like, lambda { |lead_id|
    return nil if lead_id.blank?
    lead = Lead.find(lead_id)
    if (phone = Lead.recognize_phone(lead.contact)).nil?
      where('leads.id = ?', lead_id)
    else
      where('contact LIKE ?', "%#{phone}%")
    end
  }

  scope :for_cities, lambda { |cities|
    where(:city_id => cities.keys)
  }

  scope :with_statusdate_between, lambda { |between|
    return nil if between.blank?
    from = between.split(' - ')
    where(:statusdate => from[0].to_datetime.beginning_of_day..from[1].to_datetime.end_of_day)
  }

  scope :with_footage_gt, lambda { |footage|
    where('leads.footage >= ?', footage) if footage
  }

  scope :with_footage_lt, lambda { |footage|
    where('leads.footage <= ?', footage) if footage
  }

  def self.options_for_sorted_by
    [
      ['Времени добавления', 'created_at_desc'],
      ['Статусу', 'status_asc'],
      ['Площади', 'footage_desc'],
      ['Времени планируемой работы', 'stdate_desc']
    ]
  end

  def self.to_csv(options = {force_quotes: true})
    CSV.generate(options) do |csv|
      csv << column_names
      all.each do |lead|
        csv << lead.as_json.values_at(*column_names)
      end
    end
  end

  def self.import(file)
    spreadsheet = open_spreadsheet(file)
    header = spreadsheet.row(1).map do |field|
      field.split('/').last if not field.nil?
    end
    header.reject! { |c| c.nil? }
    (2..spreadsheet.last_row).each do |i|
      row = Hash[[header, spreadsheet.row(i)[0..header.count-1]].transpose]
      product = find_by_id(row["id"]) || new
      product.attributes = row.to_hash
      product.save!
    end
  end

  def self.open_spreadsheet(file)
    case File.extname(file.original_filename)
    when ".csv" then Roo::Csv.new(file.path)
    when ".xls" then Roo::Excel.new(file.path, nil, :ignore)
    when ".xlsx" then Roo::Excelx.new(file.path, nil, :ignore)
    else raise "Неизвестный формат файла: #{file.original_filename}"
    end
  end

  def self.recognize_phone(text)
    /\d{1}-\d{3}-\d{3}-\d{2}-\d{2}/.match(text) || /\d{3}-\d{2}-\d{2}/.match(text)
  end

  def clean_attributes
    self.title = self.title.squeeze(' ')
  end

  private
    def phone_repeats_write
      if self.changed.include?("contact") || self.destroyed?
        if !(matched = Lead.recognize_phone(self.changes['contact'].try(:[], 0))).nil?
          counter = Lead.where('contact LIKE ?', "%#{matched}%").count
          Lead.where('contact LIKE ?', "%#{matched}%").update_all({'phone_repeat' => counter})
        end

        if !(matched = Lead.recognize_phone(self.contact)).nil?
          counter = Lead.where('contact LIKE ?', "%#{matched}%").count
          Lead.where('contact LIKE ?', "%#{matched}%").update_all({'phone_repeat' => counter})
        end
      end
    end

    def set_default_field_values
      self.statusdate = Time.now
      self.plan = 'Без плана' if self.plan.blank?
      self.title = 'Без названия' if self.title.blank?
    end
end