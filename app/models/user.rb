class User < ActiveRecord::Base
	has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "40x40#", :table => '30x30#' }, :default_url => "no-user-photo.png"
	has_many :user_cities
	has_many :leads, :foreign_key => 'manager_id'
	has_many :cities, :through => :user_cities
	validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/
	# Include default devise modules. Others available are:
	# :confirmable, :lockable, :timeoutable and :omniauthable
	devise :database_authenticatable, :rememberable, :trackable, :validatable

	def self.options_for_select
	  all.map { |e| ["#{e.first_name} #{e.last_name}", e.id] }
	end

	def show_city(session)
		session[:city] ||= !self.cities.first.nil? ? Hash[*self.cities.limit(1).pluck(:id, :title).flatten] : {}
	end

	def change_showed_city(session, city_id)
		if city_id == "0"
			session[:city] = Hash[*self.cities.pluck(:id, :title).flatten]
		elsif self.cities.where('user_cities.city_id = ?', city_id).count == 1
			session[:city] = Hash[*self.cities.where('user_cities.city_id = ?', city_id).pluck(:id, :title).flatten]
		end
	end

	def name
		return (!self.first_name.nil?) ? self.first_name : self.email
	end
end
