class BusinessCenter < ActiveRecord::Base
	has_many :bc_metro_distances
	has_many :metros, through: :bc_metro_distance
  has_many :business_center_region
  has_many :bc_metro_distance
  has_many :regions, :through => :business_center_region
	has_many :images, :class_name => 'Asset' , as: 'object', :dependent => :destroy
  has_many :rooms
	has_many :parse_sources
  has_many :business_center_service
  has_many :services, :through => :business_center_service
  belongs_to :city

  accepts_nested_attributes_for :images, :allow_destroy => true, :reject_if => lambda { |a| a['image'].blank? }
  accepts_nested_attributes_for :business_center_region, :allow_destroy => true, :reject_if => lambda { |a| a['region_id'].blank? }
  accepts_nested_attributes_for :bc_metro_distances, :allow_destroy => true, :reject_if => lambda { |a| a['metro_id'].blank? }
  accepts_nested_attributes_for :rooms, :allow_destroy => true, :reject_if => lambda { |a| a['area'].blank? && a['price'].blank? && a['floor'].blank? && a['room_type_id'].blank? }
  accepts_nested_attributes_for :parse_sources, :allow_destroy => true, :reject_if => lambda { |a| a['url'].blank? }
  accepts_nested_attributes_for :business_center_service, :allow_destroy => true, :reject_if => lambda { |a| a['service_id'].blank? }

  enum grade: { A: 1, B: 2, C: 3, D: 4 }
	enum percent_color: { green: 1, red: 2 }

	default_scope { order('business_centers.title')}

  after_create :slug_create

  validates :title, presence: true

	filterrific(
	  available_filters: [
	    :search_query,
      :with_rooms_area_lt,
      :with_rooms_area_gt,
      :with_rooms_price_lt,
      :with_rooms_price_gt,
	    :work_with_brokers,
	    :with_metro_distance_st,
      :with_region_id,
      :with_metro_id,
	    :with_grade,
      :with_rooms_type,
      :with_service_id,
      :with_room_ids
	  ]
	)

  include PublicActivity::Model
  tracked owner: -> (controller, model) { controller && controller.current_user },
          params:{ "obj"=> proc {|controller, model_instance|
            controller.track_params(model_instance,
              {
                "title" => nil,
                "grade" => nil,
                "description" => nil,
                "characteristics" => nil,
                "percent" => nil,
                "latitude" => nil,
                "longitude" => nil,
                "percent_color" => nil,
                "contacts" => nil,
                "short_desc" => nil,
                "address" => nil,
                "phone" => nil,
                "city_id" => lambda { |city_id|
                  City.find_by_id(city_id).nil? ? '' : City.find(city_id).title
                }
              })
          }}

  scope :search_query, lambda { |query|
    return nil if query.blank?
    query = "%#{query.to_s.downcase}%"
    where("LOWER(business_centers.title) LIKE ? OR LOWER(business_centers.address) LIKE ?", query, query)
  }

  scope :with_rooms_area_gt, lambda { |area|
    where('rooms.area >= ?', area).joins(:rooms).uniq if area
  }

  scope :with_rooms_area_lt, lambda { |area|
    where('rooms.area <= ?', area).joins(:rooms).uniq if area
  }

  scope :with_rooms_price_gt, lambda { |price|
    where('rooms.price >= ?', price).joins(:rooms).uniq if price
  }

  scope :with_rooms_price_lt, lambda { |price|
    where('rooms.price <= ?', price).joins(:rooms).uniq if price
  }

  scope :work_with_brokers, lambda { |yes|
  	where('business_centers.percent+0 <> 0') if yes.to_i > 0
  }

  scope :with_metro_distance_st, lambda { |distance|
  	where('bc_metro_distances.distance+0 <= ?', distance).joins(:bc_metro_distances).uniq if distance > 0
  }

  scope :with_region_id, lambda { |region_ids|
    region_ids = Array(region_ids)
    region_ids.reject!(&:blank?)
    where('regions.id' => [*region_ids]).joins(:regions).uniq if not region_ids.empty?
  }

  scope :with_metro_id, lambda { |metro_ids|
    metro_ids = Array(metro_ids)
    metro_ids.reject!(&:blank?)
    where('bc_metro_distances.metro_id' => [*metro_ids]).joins(:bc_metro_distances).uniq if not metro_ids.empty?
  }

  scope :with_grade, lambda { |grade|
  	where(grade: grade)
  }

  scope :with_rooms_type, lambda { |types|
    types = Array(types)
    types.reject!(&:blank?)
    where('rooms.room_type_id' => [*types]).joins(:rooms).uniq if not types.empty?
  }

  scope :with_room_ids, lambda { |rooms_ids|
    rooms_ids = Array(rooms_ids)
    rooms_ids.reject!(&:blank?)
    where('rooms.id' => [*rooms_ids]).joins(:rooms).uniq if not rooms_ids.empty?
  }

  scope :with_service_id, lambda { |services|
    services = Array(services)
    services.reject!(&:blank?)
    where('business_center_services.service_id' => [*services]).joins(:business_center_service).uniq if not services.empty?
  }

  scope :for_cities, lambda { |cities|
    where(:city_id => cities.keys)
  }

  def self.to_csv(options = {force_quotes: true})
    CSV.generate(options) do |csv|
      csv << column_names
      all.each do |business_center|
        csv << business_center.as_json.values_at(*column_names)
      end
    end
  end

  def self.import(file)
    spreadsheet = open_spreadsheet(file)
    header = spreadsheet.row(1).map do |field|
      field.split('/').last if not field.nil?
    end
    header.reject! { |c| c.nil? }
    (2..spreadsheet.last_row).each do |i|
      row = Hash[[header, spreadsheet.row(i)[0..header.count-1]].transpose]
      product = find_by_id(row["id"]) || new
      product.attributes = row.to_hash
      product.save!
    end
  end

  def self.open_spreadsheet(file)
    case File.extname(file.original_filename)
    when ".csv" then Roo::CSV.new(file.path)
    when ".xls" then Roo::Excel.new(file.path, nil, :ignore)
    when ".xlsx" then Roo::Excelx.new(file.path, nil, :ignore)
    else raise "Неизвестный формат файла: #{file.original_filename}"
    end
  end

  def zipal_grade
    zipal_grade = %w(A BPLUS B C)
    self.attributes['grade'] ? zipal_grade[self.attributes['grade']-1] : 'B'
  end

  def parse
    self.rooms.parsed.delete_all
    if (parse_source = self.parse_sources.active.first)
      parsed = parse_source.parse
      self.rooms << parsed[:rooms]
      self.services.delete_all
      self.business_center_service << parsed[:business_center_service]
    end
    self.touch(:parsed_at)
    return parse_source
  end

  def self.parseAll
    BusinessCenter.where('parsed_at < ? OR parsed_at IS NULL', 1.week.ago).limit(4).each do |bc|
      bc.parse
    end
  end

  def slug_create
    id = self.id
    if self.slug.empty?
      self.slug = "#{id}-#{self.title.parameterize}"
    else
      self.slug = "#{id}-#{self.slug}"
    end
    self.save
  end
end