class Room < ActiveRecord::Base
	belongs_to :room_type
	belongs_to :business_center

	self.inheritance_column = nil

	def self.emls_regions
		{
			38 => 'Адмиралтейский',
			43 => 'Василеостровский',
			4 => 'Выборгский',
			6 => 'Калининский',
			7 => 'Кировский',
			8 => 'Красногвардейский',
			9 => 'Красносельский',
			12 => 'Московский',
			13 => 'Невский',
			20 => 'Петроградский',
			14 => 'Приморский',
			15 => 'Фрунзенский',
			39 => 'Центральный'
		}
	end

	scope :parsed, lambda { where(parsed: true) }
	scope :emls_region, lambda {
		joins(business_center: :regions)
			.where('regions.title IN (?)', self.emls_regions.values) 
	}

	def cian_room_type
		room_type = 'Офисное помещение'
		room_type = 'Складское помещение' if self.room_type && self.room_type.title == 'склад'
		room_type = 'Торговое помещение' if (self.room_type && ( self.room_type.title == 'торговое' || self.room_type.title == 'Торг.пом.'))
		room_type = 'Помещение свободного назначения' if self.room_type && self.room_type.title == 'Унив.пом.'
		return room_type
	end

	def cian_bc_grade
		cian_grade = %w(A B C D)
		grade = nil
		if self.cian_room_type == 'Офисное помещение' || self.cian_room_type == 'Складское помещение'
			grade = self.business_center.attributes['grade'] ? cian_grade[self.business_center.attributes['grade']-1] : 'B'
		end
		return grade
	end

	def type
		self.room_type ? self.room_type.title.slice(0,1).mb_chars.upcase + self.room_type.title.slice(1..-1) : 'Офис'
	end
end
