class BusinessCenterRegion < ActiveRecord::Base
  belongs_to :business_center
  belongs_to :region
end