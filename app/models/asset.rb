class Asset < ActiveRecord::Base
	has_attached_file :image, :styles => {
    :medium => "170x170#",
    :thumb => "40x40#",
    :api_thumb => "200x200#",
    :api_full => "1200x400>"
    }, :default_url => "no-photo-bc.jpg"
	validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
	validates_attachment_presence :image
	belongs_to :object, polymorphic: true
end
