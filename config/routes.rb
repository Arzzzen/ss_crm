Rails.application.routes.draw do

  resources :pages

  resources :services

  # constraints(:ip => /127.0.0.1/) do
    get 'api/business_centers'
    get 'api/cities'
    get 'api/regions'
    get 'api/metros'
    get 'api/room_types'
    get 'api/services'
    get 'api/business_center/:id' => 'api#business_center'
    get 'api/business_centers_for_map'
    get 'api/similar_business_centers/:id' => 'api#similar_business_centers'
    get 'api/page/:id' => 'api#page'
  # end

  post 'users/change_showed_city' => 'users#change_showed_city', as: 'change_showed_city'

  devise_for :users
  resources :cities
  resources :activities
  resources :notifications
  resources :lead_sources
  resources :room_types
  resources :leads
  resources :business_centers do
    collection {
      post :import
      get :parse_test
      get ':id/parse' => 'business_centers#parse', as: 'parse'
    }
  end
  resources :users
  resources :leads do
    collection do
      post :import
    end
  end
  resources :statuses
  resources :room_types

  get 'admin' => 'admin#index'
  get 'statistics' => 'admin#statistics'
  get 'zipal' => 'business_centers#zipal', as: 'zipal'
  get 'cian' => 'business_centers#cian', as: 'cian'
  get 'emls' => 'business_centers#emls', as: 'emls'
  get 'leads_select' => 'leads#select'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'leads#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
